CSG
===
A simple Constructive Solid Geometry library, in the style of openScad or csg.js. 

Currently, this isn't very efficient, and generates meshes with an excessive number of triangles. 

![An example of the cube from the Wikipedia page on CSG, with far too many polygons](imgs/render.jpg)

It can be used to create real printable models:

![An image of a neon colored cube like object, made with this library](imgs/print.jpg)

Build
-----

This project uses cabal, install into a sandbox with:
```
cabal sandbox init
cabal install -j
```

Usage
-----
  
See the examples folder, specifically CAD\_example.hs, for examples regarding how to use this library to build shapes.

