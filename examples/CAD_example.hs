{-# LANGUAGE TypeSynonymInstances #-}
{-# LANGUAGE FlexibleInstances #-}
import qualified Data.Text.IO as T
import qualified Csg
import qualified Csg.STL
import Data.Serialize
import Data.Semigroup
import Data.Vec3 as V3

object :: Csg.BspTree
object = (cube `Csg.subtract` cross) `Csg.intersection` sphere
    where 
        sphere = Csg.unitSphere 32 16
        cube = Csg.uniformScale 1.6 Csg.unitCube
        cylinder = Csg.scale (0.5, 0.5, 3.0) $ Csg.unitCylinder 32
        axes = [(1.0, 0.0, 0.0), (0.0, 1.0, 0.0), (0.0, 0.0, 1.0)]  
        cross = foldl1 Csg.union $ map (\a -> Csg.rotate a (pi/2) cylinder) axes

mapTuple3 :: (a -> b) -> (a, a, a) -> (b, b, b)
mapTuple3 f (a1, a2, a3) = (f a1, f a2, f a3)

path = "out.stl"

main :: IO ()
main = T.writeFile path $ Csg.STL.toSTL object
