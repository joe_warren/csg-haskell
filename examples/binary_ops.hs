{-# LANGUAGE TypeSynonymInstances #-}
{-# LANGUAGE FlexibleInstances #-}
import qualified Data.ByteString as BS
import qualified Data.Text as T
import qualified Csg 
import Data.Serialize
import Options.Applicative
import Data.Semigroup
import Graphics.Formats.STL
import Data.Vec3 as V3

type MergeOp = (Csg.BspTree -> Csg.BspTree -> Csg.BspTree)
data Opts = Opts String String String MergeOp

opts :: Parser Opts
opts = Opts 
    <$> strArgument 
        (metavar "FILE1" 
        <> help "Input STL file")
    <*> strArgument 
        (metavar "FILE2" 
        <> help "Input STL file")
    <*> strArgument 
        (metavar "Output" 
        <> help "Output STL")
    <*> (flag' Csg.union (long "union") <|> flag' Csg.subtract (long "subtract") <|> flag' Csg.intersection (long "intersection"))

type Tri = (V3.CVec3, V3.CVec3, V3.CVec3)

mergeTris :: [Tri] -> [Tri] -> [Tri]
mergeTris = (++) 

mapTuple3 :: (a -> b) -> (a, a, a) -> (b, b, b)
mapTuple3 f (a1, a2, a3) = (f a1, f a2, f a3)

extractTris :: STL -> [Tri]
extractTris s = map ( mapTuple3 ( V3.fromXYZ . mapTuple3 realToFrac ) . vertices) $ triangles s   

insertTris :: [Csg.Tri] -> [Triangle]
insertTris = map (conv . mapTuple3 (mapTuple3 realToFrac . V3.toXYZ))
    where conv t = Triangle{ normal=Nothing, vertices=t }

merge :: MergeOp -> STL -> STL -> STL
merge op a b = STL {name=T.pack "Merged", triangles= t}
    where 
        aTree = Csg.fromTris $ extractTris a
        bTree = Csg.fromTris $ extractTris b
        t =  insertTris $ Csg.toTris $ op aTree bTree

mergeSTLs :: Opts -> IO ()
mergeSTLs (Opts fn1 fn2 on mergeOp) = do
    f1 <- BS.readFile fn1
    f2 <- BS.readFile fn2
    case liftA2 (,) (getStl f1 ) (getStl f2) of
        Left err -> do
            putStrLn $ "Encountered error reading "++fn1
            putStrLn err
        Right (stl1, stl2) -> do
            BS.writeFile on . runPut . put $ merge mergeOp stl1 stl2
            putStrLn $ "wrote output to " ++ on
    where getStl = runGet (get :: Get STL)

main :: IO ()
main = execParser withHelp >>= mergeSTLs where
  withHelp = info (helper <*> opts)
               ( fullDesc <> progDesc "read  FILE1 and FILE2 STL, and write out \"roundtrip.stl\", also as binary STL"
                 <> header "binary-roundtrip - a test for STL library" )
