{-# LANGUAGE TypeSynonymInstances #-}
{-# LANGUAGE FlexibleInstances #-}
import qualified Data.Text.IO as T
import qualified Csg
import qualified Csg.STL
import Data.List
import Data.Serialize
import Data.Semigroup
import Data.Vec3 as V3
dieFaces :: [[[Int]]]
dieFaces = [[
    [0, 0, 0],
    [0, 1, 0],
    [0, 0, 0]],

   [[1, 0, 0], 
    [0, 0, 0], 
    [0, 0, 1]], 

   [[1, 0, 0], 
    [0, 1, 0], 
    [0, 0, 1]], 

   [[1, 0, 1], 
    [0, 0, 0], 
    [1, 0, 1]], 

   [[1, 0, 1], 
    [0, 1, 0], 
    [1, 0, 1]], 

   [[1, 0, 1], 
    [1, 0, 1], 
    [1, 0, 1]]]

reifyFace :: Csg.BspTree -> [[Int]] -> Csg.BspTree
reifyFace shape f = foldl1 Csg.union shapes
    where 
         inds = concatMap (\(i, js) -> map (\j->(fromIntegral i, fromIntegral j)) js) $ zip [0..] $ map (elemIndices 1) f
         shapes = map (\(i, j)-> Csg.translate (fromIntegral i - 1.0, fromIntegral j - 1.0, 0.0) shape) inds

axisX = (1.0, 0.0, 0.0)
axisY = (0.0, 1.0, 0.0)
axisZ = (0.0, 0.0, 1.0)

rotations :: [Csg.BspTree -> Csg.BspTree]
rotations = [
    Csg.rotate axisX 0.0,
    Csg.rotate axisY (pi/2), 
    Csg.rotate axisX (pi/2), 
    Csg.rotate axisX (-pi/2), 
    Csg.rotate axisY (-pi/2),
    Csg.rotate axisX pi
    ]

object :: Csg.BspTree
object = combinedFaces `Csg.intersection` sphere
    where 
        holeShape = Csg.uniformScale 0.38 $ Csg.unitCone 16
        facePatterns = map (reifyFace holeShape) dieFaces
        translateFaceIntoPlace = Csg.translate (0.0, 0.0, -0.75) . Csg.uniformScale 0.35
        positionedPatterns = map (\(r, f) -> r f) $ zip rotations $ map translateFaceIntoPlace facePatterns
        cube = Csg.uniformScale 1.5 Csg.unitCube
        combinedFaces = foldl Csg.subtract cube positionedPatterns
        sphere = Csg.unitSphere 32 16

path = "dice.stl"

main :: IO ()
main = T.writeFile path $ Csg.STL.toSTL object
