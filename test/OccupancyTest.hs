module Main where

import Test.HUnit
import qualified Csg
import System.Exit

test1 :: Test
test1 = TestCase (assertEqual "cube contains origin" (Csg.encloses Csg.unitCube (0.0, 0.0, 0.0)) (True))

test2 :: Test
test2 = TestCase (assertEqual "cube doesn't contain exterior point" (Csg.encloses Csg.unitCube (1.0, 0.0, 0.0)) (False))

tests :: Test
tests = TestList [TestLabel "cube contains origin" test1, 
                  TestLabel "cube doesn't contain exterior point" test2]

main :: IO Counts
main = do
    cs@(Counts _ _ errs fails) <- runTestTT tests
    putStrLn (showCounts cs)
    if (errs > 0 || fails > 0) 
        then exitFailure
        else exitSuccess
