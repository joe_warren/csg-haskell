{-# LANGUAGE TypeSynonymInstances #-}
{-# LANGUAGE FlexibleInstances #-}
module Csg.STL
( toSTL
) where

import Csg
import Data.Vec3 as V3
import Data.List
import qualified Data.Text as T

tuple3List :: (a, a, a) -> [a]
tuple3List (x, y, z) = [x, y, z] 

decompose :: Double -> (Double,Int)
decompose val = if mant2 > 0 
                     then (mant10,ex10)
                     else (-mant10,ex10)
  where
        (mant2,ex2) = decodeFloat val
        res = logBase 10 (fromIntegral (abs mant2)::Double) + logBase 10 (2 ** (fromIntegral ex2::Double)) 
        ex10 = floor res
        mant10 = 10**(res - (fromIntegral ex10::Double))

ingen :: Double -> (Double,Int)
ingen val 
  | mod ex 3 == 0 = (mant,ex)
  | mod ex 3 == 1 = (mant*10,ex-1)
  | mod ex 3 == 2 = (mant*100,ex-2)
  where
        (mant,ex) = decompose val

showEng :: Double -> String
showEng a = (show mant) ++ "e" ++ (show ex)
     where 
        (mant, ex) = decompose a

vertexString :: V3.CVec3 -> T.Text
vertexString x = T.pack ("        vertex " ++ (showEng a) ++ " " ++ (showEng b) ++ " " ++ (showEng c) ++ "\n")
    where 
        (a, b, c) =toXYZ x

triangleText :: Csg.Tri -> T.Text
triangleText (a, b, c) = T.concat [normalText, ol, as, bs, cs, el, end]
    where
        (nx, ny, nz) = toXYZ $ normalize $ (b <-> a) >< (c <-> a)
        normalText = T.pack("facet normal " ++ (showEng nx) ++ " "++ (showEng ny) ++ " " ++ (showEng nz) ++ "\n")
        ol = T.pack "    outer loop\n"
        as = vertexString a
        bs = vertexString b
        cs = vertexString c
        el = T.pack "    endloop\n"
        end = T.pack "endfacet\n"

toSTL :: BspTree -> T.Text
toSTL t = T.concat [start, body, end]
    where 
        tris = toTris t
        start = T.pack "solid csg-result\n"
        body = T.concat $ map triangleText tris
        end = T.pack "endsolid csg-result\n"
