{-# LANGUAGE TypeSynonymInstances #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE BangPatterns #-}
module Csg
( BspTree (..)
, Tri
, clip
, flipped
, fromTris
, toTris
, emptyBspTree
, Csg.subtract
, intersection
, Csg.encloses
, Csg.union
, Intersection (..)
, fromIntersection
, intersectionConcat
, Union (..)
, fromUnion
, unionConcat
, unitCube
, unitSphere
, unitCylinder
, unitCone
, uniformScale
, aabb
, center
, scale
, rotate
, translate
) where

import Data.Vec3 as V3
import Data.List
import Data.Bits (testBit)
import Data.Monoid
import Data.Semigroup
import Data.List.NonEmpty (NonEmpty (..))
import qualified Data.List.NonEmpty as NonEmpty

type Vector = V3.CVec3

type Poly = [Vector]

type Tri = (Vector, Vector, Vector)

polyToTris :: Poly -> [Tri]
polyToTris (a:b:c:xs) = (a, b, c) : polyToTris (a:c:xs)
polyToTris _ = []

triToPoly :: Tri -> Poly
triToPoly (a, b, c) = [a,b,c]

class Flippable a where
    flipped :: a -> a

instance Flippable Tri where
    flipped (a, b, c) = (c, b, a)

data Plane = Plane {
    planeNormal :: Vector,
    planeW :: Double
} deriving Eq

triToPlane :: Tri -> Plane 
triToPlane (a, b, c) = Plane n (n .* a)
    where n = normalize $ (b <-> a) >< (c <-> a)

data Side = FRONT | BACK | SPANNING | COPLANAR deriving Eq

combineSides :: Side -> Side -> Side
combineSides FRONT FRONT = FRONT
combineSides BACK BACK = BACK
combineSides FRONT BACK = SPANNING
combineSides COPLANAR x = x
combineSides SPANNING _ = SPANNING
combineSides x y = combineSides y x

epsilon = 1e-5

vectorSideOfPlane :: Plane -> Vector -> Side
vectorSideOfPlane (Plane n w) v = case t of 
                                   x | t < -epsilon -> BACK
                                     | t > epsilon -> FRONT
                                     | otherwise -> COPLANAR 
    where t = n .* v - w 

sideOfPlane :: Plane -> Tri -> Side
sideOfPlane p (a, b, c) = s a `combineSides` s b `combineSides` s c 
    where s = vectorSideOfPlane p

vectorLerp :: Vector -> Vector -> Double -> Vector
vectorLerp a b t = a <+> ((b <-> a) .^ t)

mapTuple :: (a -> b) -> (a, a) -> (b, b)
mapTuple f (a, b) = (f a, f a)

map3Tuple :: (a -> b) -> (a, a, a) -> (b, b, b)
map3Tuple f (a, b, c) = (f a, f b, f c)   

map4Tuple :: (a -> b) -> (a, a, a, a) -> (b, b, b, b)
map4Tuple f (a, b, c, d) = (f a, f b, f c, f d)   

splitSpanningTriangle :: Plane -> Tri -> (Poly, Poly)
splitSpanningTriangle p (a, b, c) = (front, back)
    where points = [a,b,c]
          edges = [(a,b),(b,c),(c,a)]
          splitSpanningEdgeFst (a, b) = case side a of
                                            FRONT -> ([a], [])
                                            BACK -> ([], [a])
                                            COPLANAR -> ([a], [a])
          splitSpanningEdgeSnd (a, b) = case side a `combineSides` side b of
                                            SPANNING -> ([lerp a b], [lerp a b])
                                            _ -> ([], [])
          splitSpanningEdge e =  case splitSpanningEdgeFst e of
                                    (f1, b1) -> case splitSpanningEdgeSnd e of
                                                    (f2, b2) -> (f1++f2, b1++b2)
          spanning = map splitSpanningEdge edges
          front = concatMap fst spanning
          back = concatMap snd spanning
          lerp a b = vectorLerp a b $ t a b
          t a b = (w - n .* a) / (n .* (b <-> a))
          w = planeW p
          n = planeNormal p
          side = vectorSideOfPlane p

-- returns polygons that are coplanarFront, coplanarBack, front and back
-- respective to the plane
splitTriWithPlane :: Plane -> Tri -> ([Tri], [Tri], [Tri], [Tri])
splitTriWithPlane (Plane n w) t = 
    case side of
        FRONT -> ([], [], [t], [])
        BACK -> ([], [], [], [t])
        COPLANAR | coplanarDot > 0.0 -> ([t], [], [], [])
                 | otherwise -> ([], [t], [], [])
        SPANNING -> case splitSpanningTriangle (Plane n w) t of
                        (front, back) -> ([], [], polyToTris front, polyToTris back)
    where side = sideOfPlane (Plane n w) t
          coplanarDot = n .* planeNormal (triToPlane t)

data BspTree = BspBranch !(NonEmpty Tri) !BspTree !BspTree | EmptyBspTree

emptyBspTree = EmptyBspTree

addToTree :: BspTree -> [Tri] -> BspTree
addToTree tree [] = tree
addToTree (BspBranch (head :| coplanar) front back) tris = 
    BspBranch newCoplanar (addToTree front newFront) (addToTree back newBack)
    where 
        split = splitTriWithPlane $ triToPlane head
        (coplanarFront, coplanarBack, newFront, newBack) = map4Tuple concat $ unzip4 $ map split tris
        newCoplanar = head :| coplanar ++ coplanarFront ++ coplanarBack
addToTree EmptyBspTree (head:tris) = 
    BspBranch coplanar (addToTree EmptyBspTree newFront) (addToTree EmptyBspTree newBack)
    where 
        split = splitTriWithPlane $ triToPlane head
        (coplanarFront, coplanarBack, newFront, newBack) = map4Tuple concat $ unzip4 $ map split tris
        coplanar = head :| (coplanarFront ++ coplanarBack)


encloses :: BspTree -> (Double, Double, Double) -> Bool
encloses EmptyBspTree _ = False
encloses tree vd = encloses' tree
    where 
        v = V3.fromXYZ vd :: Vector
        encloses' (BspBranch (t:|_) front back) =
            case vectorSideOfPlane (triToPlane t) v of
                FRONT -> case front of
                            EmptyBspTree -> False
                            a -> encloses' front
                BACK -> case back of
                            EmptyBspTree -> True
                            a -> encloses' back
                COPLANAR -> case back of
                            EmptyBspTree -> True
                            a -> encloses' back

clipTris :: Bool -> BspTree -> [Tri] -> [Tri]
clipTris icf EmptyBspTree tris = tris
clipTris icf (BspBranch (head:|xs) front back) tris = newFront ++ newBack
    where 
        split = splitTriWithPlane $ triToPlane head
        (coplanarFront, coplanarBack, splitFront, splitBack) = map4Tuple concat $ unzip4 $ map split tris
        newFront = clipTris icf front ((if icf then coplanarFront else []) ++ splitFront)
        newBack = case back of
                    EmptyBspTree -> []
                    (BspBranch t _ _) -> clipTris icf back (coplanarBack++splitBack++(if icf then [] else coplanarFront))

clip :: Bool -> BspTree -> BspTree -> BspTree 
clip _ EmptyBspTree _ = EmptyBspTree
clip icf (BspBranch tris front back) clipTree =
    case NonEmpty.nonEmpty $ clipTris icf clipTree (NonEmpty.toList tris) of
        Nothing -> fromTris $ clipTris icf clipTree (toTris front ++ toTris back)
        Just clipped -> case normalOf tris .* normalOf clipped of
                            x | x > 0.0 -> BspBranch clipped (clip icf front clipTree) (clip icf back clipTree)
                              | otherwise -> BspBranch clipped (clip icf back clipTree) (clip icf front clipTree)
    where normalOf t = planeNormal $ triToPlane $ NonEmpty.head t

instance Flippable BspTree where
    flipped (BspBranch tris front back) = BspBranch (NonEmpty.map flipped tris) (flipped back) (flipped front)
    flipped EmptyBspTree = EmptyBspTree

clipWithFront :: BspTree -> BspTree -> BspTree
clipWithFront = clip True
clipWithoutFront :: BspTree -> BspTree -> BspTree
clipWithoutFront = clip False 

mapTris :: (Tri -> Tri) -> BspTree -> BspTree
mapTris f (BspBranch tris front back) = 
    BspBranch (NonEmpty.map f tris) (mapTris f front) (mapTris f back)
mapTris _ EmptyBspTree = EmptyBspTree

mapPoints :: (Vector -> Vector) -> BspTree -> BspTree
mapPoints f = mapTris (map3Tuple f)

matrixTransform :: Matrix Vector -> BspTree -> BspTree
matrixTransform m = mapPoints (mxv m)

mxd :: Matrix Vector -> Double -> Matrix Vector 
mxd m d = fromRows $ map3Tuple (.^ d) (toRows m)

-- This makes literally no sense unless you're looking at the equation 
-- for the matrix of rotation around an axis
rotate :: (Double, Double, Double) -> Double -> BspTree -> BspTree
rotate axis angle = matrixTransform m 
    where 
        u = normalize $ V3.fromXYZ axis :: Vector
        uxu = u `vxv` u
        ux = case toXYZ u of
                (u1, u2, u3) -> fromRows ( fromXYZ ( 0, -u3, u2),
                                           fromXYZ (u3, 0, -u1), 
                                           fromXYZ (-u2, u1, 0) ) :: Matrix Vector
        i = diag 1 :: Matrix Vector
        m = (i `mxd` cos angle) `addM` (ux `mxd` sin angle) `addM` (uxu `mxd` (1.0 - cos angle))


scale :: (Double, Double, Double) -> BspTree -> BspTree
scale vt t = flipMaybe $ mapPoints (V3.zipWith (*) v) t
    where 
        v = V3.fromXYZ vt
        flipMaybe = case vt of
                        (x, y, z) | x*y*z < 0.0 -> mapTris flipped 
                                  | otherwise -> id

uniformScale :: Double -> BspTree -> BspTree
uniformScale v = scale (v, v, v)

translate :: (Double, Double, Double) -> BspTree -> BspTree
translate vt = mapPoints (V3.zipWith (+) v)
    where
        v = V3.fromXYZ vt

subtract :: BspTree -> BspTree -> BspTree
subtract a b =  addToTree aClipped $ toTris bClipped
    where 
        aClipped = flipped $ clipWithFront (flipped a) b
        bClipped = flipped $ clipWithoutFront b (flipped a)
      
intersection :: BspTree -> BspTree -> BspTree 
intersection a b = addToTree aClipped $ toTris bClipped
    where
        bClipped = clipWithFront b $ flipped a
        aClipped = clipWithoutFront a $ flipped b

union :: BspTree -> BspTree -> BspTree
union a b = addToTree aClipped $ toTris bClipped
    where
        aClipped = clipWithFront a b
        bClipped = clipWithoutFront b a

unitCube :: BspTree
unitCube = fromTris $ concatMap polyToTris polys 
    where 
        inds = [
            [0, 4, 6, 2], 
            [1, 3, 7, 5], 
            [0, 1, 5, 4], 
            [2, 6, 7, 3], 
            [0, 2, 3, 1], 
            [4, 5, 7, 6]] :: [[Int]]
        axisVal axis i = if testBit i axis then 0.5 else -0.5
        point i = (axisVal 0 i,  axisVal 1 i , axisVal 2 i ) 
        polys = map (map (V3.fromXYZ . point)) inds

unitSphere :: Int -> Int -> BspTree
unitSphere slices stacks = fromTris (topFan++bottomFan++midsection)
    where 
        slicesD = fromIntegral slices :: Double
        stacksD = fromIntegral stacks :: Double
        topVert = V3.fromXYZ (0.0, 0.0, 1.0) :: Vector
        bottomVert = V3.fromXYZ (0.0, 0.0, -1.0) :: Vector
        vertex theta phi = V3.fromXYZ (cos theta * sin phi, sin theta * sin phi, cos phi) :: Vector
        v i j = vertex (2.0 * i * pi /slicesD) (pi * j /stacksD)
        fanLoop j = map (`v` j) [0..]
        topFanLoop = fanLoop 1
        bottomFanLoop = fanLoop (stacksD-1)
        fanEdges loop = take slices $ zip loop $ tail loop
        topFanEdges = fanEdges topFanLoop
        bottomFanEdges = fanEdges bottomFanLoop
        topFan = map (\(a, b) -> (topVert, a, b)) topFanEdges
        bottomFan = map (\(a, b) -> (bottomVert, b, a)) bottomFanEdges
        squareAt i j = [v i j, v i (j+1), v (i+1) (j+1), v (i+1) j] 
        midsection = concatMap polyToTris [squareAt (fromIntegral i) (fromIntegral j) | i <- [0..slices], j <-[1..stacks-1]]

unitCylinder :: Int -> BspTree
unitCylinder slices = fromTris (polyToTris top ++ polyToTris (reverse bottom) ++ sides)
    where
        slicesD = fromIntegral slices :: Double
        theta i = 2.0*i*pi/slicesD
        v i z = V3.fromXYZ (cos $ theta i, sin $ theta i, z)
        top= [v (fromIntegral i) 0.5 | i <- [0..slices]]
        bottom = [v (fromIntegral i) (-0.5) | i <- [0..slices]]
        collected = zip top bottom
        sides = concatMap (polyToTris.(\((a, b), (c, d))-> [a, b, d, c])) $ zip collected $ tail $ cycle collected

unitCone :: Int -> BspTree
unitCone slices = fromTris (polyToTris (reverse bottom) ++ sides)
    where 
        slicesD = fromIntegral slices :: Double
        theta i = 2.0*i*pi/slicesD
        v i = V3.fromXYZ (cos $ theta i, sin $ theta i, -0.5) :: Vector
        bottom = [v (fromIntegral i) | i <- [0..slices]]
        top = V3.fromXYZ (0.0, 0.0, 0.5) :: Vector
        sides = map (\(a, b)-> (a, b, top)) $ zip bottom $ tail $ cycle bottom
 
tuple3List :: (a, a, a) -> [a]
tuple3List (x, y, z) = [x, y, z] 

aabbLower :: BspTree -> (Double, Double, Double)
aabbLower t = toXYZ $ foldl1 (V3.zipWith min) $ concatMap tuple3List $ toTris t

aabbHigher :: BspTree -> (Double, Double, Double)
aabbHigher t = toXYZ $ foldl1 (V3.zipWith max) $ concatMap tuple3List $ toTris t
       
aabb :: BspTree -> ((Double, Double, Double), (Double, Double, Double))
aabb t = (aabbLower t, aabbHigher t)

center :: BspTree -> BspTree 
center o = translate (-mx, -my, -mz) o
  where
    ((x1, y1, z1), (x2, y2, z2)) = aabb o
    mx = (x1 + x2)/2
    my = (y1 + y2)/2
    mz = (z1 + z2)/2

fromTris :: [Tri] -> BspTree
fromTris = addToTree EmptyBspTree

toTris :: BspTree -> [Tri]
toTris EmptyBspTree = []
toTris (BspBranch tris front back) = NonEmpty.toList tris ++ toTris front ++ toTris back

newtype Union = Union Csg.BspTree
instance Semigroup Union where
  Union x <> Union y = Union (x `Csg.union` y)
instance Monoid Union where 
  mempty = Union Csg.EmptyBspTree

fromUnion :: Union -> Csg.BspTree
fromUnion (Union x) = x

unionConcat :: [BspTree] -> BspTree
unionConcat xs = Csg.fromUnion $ mconcat $ map Csg.Union xs

data Intersection = Intersect Csg.BspTree | Everywhere
instance Semigroup Intersection where
  Intersect x <> Intersect y = Intersect (x `Csg.intersection` y)
  Intersect x <> Everywhere = Intersect x
  Everywhere <> Intersect y = Intersect y
  Everywhere <> Everywhere = Everywhere

instance Monoid Intersection where 
  mempty = Everywhere

fromIntersection :: Intersection -> Csg.BspTree
fromIntersection (Intersect x) = x
fromIntersection Everywhere = Csg.EmptyBspTree

intersectionConcat :: [BspTree] -> BspTree
intersectionConcat xs = Csg.fromIntersection $ mconcat $ map Intersect xs
